#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 15:37:31 2018

@author: lk
"""

import os,sys
from os import listdir
import re

import pandas as pd 
from gensim.models import Doc2Vec
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords 
import email

from sklearn.externals import joblib


lemma = WordNetLemmatizer()
stops = set(stopwords.words('english'))
stops.update(("to","cc","subject","http","from","sent"))
numbers = '\S*\d+\S*'
links = 'http:\S*\s'
names = ' [A-Z][a-z]+'
forwards = '-----Original Message-----(.*\n){6}'

def clean(text):
    text = re.sub(numbers, 'num', text)
    text = re.sub(forwards, ' ', text)
    text = re.sub(links, 'http ', text)
    text = re.sub(names, ' name', text)
    text = text.lower().strip()
    text = re.sub(r"[^a-zA-Z'$]", ' ', text)
    stop_free = [i for i in text.split() if((i not in stops) )]
    normalized = [lemma.lemmatize(word) for word in stop_free]    
    return normalized


def get_files(path):
    t = pd.DataFrame(columns = ['text', 'path'])
    i = 0
    for file in listdir(path):
        try:
            filepath = os.path.join(path,file)
            f = open(filepath,'r')
            m = email.message_from_string(f.read())
            t.loc[i] =  m.get_payload(), filepath
            i+=1

        except Exception:
            print(filepath)
    return t



model_vec = Doc2Vec.load('model_vec')
clf = joblib.load('clf')
t = get_files(sys.argv[1])
t['cleaned'] = t['text'].apply(clean)
t['vectors'] = t['text'].apply(lambda x: model_vec.infer_vector(clean(x)))
t['results'] =  clf.predict(list(t['vectors']))
t['results'] = t['results'].apply(lambda x: 'business' if x else 'personal')
t[['path','results']].to_csv('results.csv',index = False)