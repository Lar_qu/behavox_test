#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from os import listdir
import re
from random import shuffle

import pandas as pd 
from gensim.models import Doc2Vec
from gensim.models import doc2vec
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords 
import email
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import PCA
from nltk.stem.porter import PorterStemmer
from sklearn import svm
from sklearn.externals import joblib

#nltk.download('wordnet')
#nltk.download('stopwords')

path ='.'
porter = PorterStemmer()
lemma = WordNetLemmatizer()
stops = set(stopwords.words('english'))
stops.update(("to","cc","subject","http","from","sent","fw"))
#регекспы для замены на одно слово
numbers = '\S*\d+\S*'
links = 'http:\S*\s'
#тут попыталась исключить попадание дней недели в группу имен.
names = ' [A-Z][a-z]*[a-xz]\b'
price = '\$\d+'
#это нужно просто убрать
forwards = '-----Original Message-----(.*\n){6}'


def get_mails():
#    создать пустую таблицу в которую буду складывать файлы
    t = pd.DataFrame(columns = ['text','class','path'])
    i = 0
    for folder in ['business', 'personal']:
        for file in listdir(os.path.join(path,folder)):
            try:
                filepath = os.path.join(folder,file)
                f = open(filepath,'r')
#                попробовать вытащить емейл
                m = email.message_from_string(f.read())
#                убрать разметку
                mail_body = re.split('\</?MARKUP(.*e)?\>', m.get_payload())
                if 'Subject' in m.keys():
#                    темы могут содержать ключевые слова, поэтому тоже возьмем
                    t.loc[i] = m['Subject'], folder, filepath
                    i+=1
#                положить в таблицу письмо, класс, путь к файлу
                t.loc[i] =  mail_body[0], folder, filepath
                i+=1

            except Exception:
#                если что то пошло не так - показать в каком файл
                print(filepath)
    return t


def clean(text):
#    заменить по регекспу
    text = re.sub(price, 'price', text)
    text = re.sub(numbers, 'num', text)
    text = re.sub(forwards, ' ', text)
    text = re.sub(links, 'http ', text)
    text = re.sub(names, ' name ', text)
    text = text.lower().strip()
#    убрать символы
    text = re.sub(r"[^a-zA-Z'$]", ' ', text)
#    убрать стоп слова
    stop_free = [i for i in text.split() if((i not in stops) )]
#    привести к начальной форе
    normalized = [lemma.lemmatize(word) for word in stop_free]    
#    normalized = [porter.stem(word) for word in normalized]
    return normalized

#разметка сообщений для doc2vec
def label_sentences(table):
    for indx, row in table.iterrows():
        yield doc2vec.TaggedDocument(row['cleaned'], [indx,row['class']])



t = get_mails()
t['cleaned'] = t['text'].apply(clean)
sent = list(label_sentences(t))
shuffle(sent)

#обучение doc2vec
model = Doc2Vec(vector_size=200, min_count=1, alpha=0.025, min_alpha=0.02)
model.build_vocab(sent)
model.train(sent,total_examples=model.corpus_count, epochs = 2)

#
#надо вытащить все вектора из модели
train_array = []
for i in range(model.corpus_count):
    train_array.append(model[i])

#dimension reduction for plotting
pca = PCA(n_components=2)
pca.fit(train_array)
X = pca.transform(train_array)

#создание таблицы по уменьшенным векторам для отрисовки
tr = pd.DataFrame(X)
tr['class'] = t['class']
tr.columns = ['x1','x2','class']
sns.lmplot('x1','x2',data = tr, hue = 'class',fit_reg=False,\
           scatter_kws={'alpha':0.3})
plt.show()


t['bool'] = t['class'].apply(lambda x: int(x == 'business'))
b = tr[tr['class'] == 'business']
indxes = b[b['x1'].between(-1,1,5) & b['x2'].between(-1,1)].index

train_array1 = [value for counter, value in enumerate(train_array) \
                if counter not in indxes]

#didn't work()
#clf = svm.SVC(degree = 4, class_weight = {0:1,1:1.6888216},gamma =1e-8)
clf = svm.LinearSVC()
clf.fit(train_array1, t['bool'].drop(indxes))
print("fitted with score %0.2f" %(clf.score(train_array1,t['bool'].drop(indxes))))

test = pd.read_csv('mobile_meta.csv')
test = test[test['category'] != 'e']
test['category'] = test['category'].apply(lambda x: 1 if x =='b' else 0)
test['vectors'] = test['text'].apply(lambda x: model.infer_vector(clean(x)))
print("test score %0.2f" %(clf.score(list(test['vectors']),test['category'])))

model.save('model_vec')
joblib.dump(clf,'clf')
